package com.inkglobal.techtest;

import java.util.StringTokenizer;

public class BerlinClock {

	public BerlinClock() {}

	
	final static char YELLOW = 'Y';
	final static char OFF = 'O';
	final static char RED = 'R';

	
	public static String printBerlinClockStyleTime(String time){
		
	  StringTokenizer timeTokens = new StringTokenizer(time, ":");	
	  String parseHours = (String)timeTokens.nextToken();
	  String parseMinutes = (String)timeTokens.nextToken();
	  String parseSeconds = (String)timeTokens.nextToken();
	  
		
	  return String.format("%1$s %2$s %3$s",seconds(Integer.parseInt(parseSeconds)), hours(Integer.parseInt(parseHours)), minutes(Integer.parseInt(parseMinutes)));
	
	}
	
	static String hours(int hours){
		char[] chars = new char[9];
		int blocksOfFiveHours = hours / 5;
		int remainingHours = hours % 5;
		for(int i = 0;i < 4; i++){
			if(blocksOfFiveHours > 0){
				chars[i] = RED;
			} else {
				chars[i] = OFF;
			}
			blocksOfFiveHours--;
		}
		chars[4] = ' ';
		for(int i = 5;i < 9; i++){
			if(remainingHours > 0){
				chars[i] = RED;
			} else {
				chars[i] = OFF;
			}
			remainingHours--;
		}
		return new String(chars);
	}
	
	static String seconds(int seconds){
		if(seconds % 2 == 0){
			return new Character(YELLOW).toString();
		} else {
			return new Character(OFF).toString();

		}
		
	}
	
	static String minutes(int mins){
		char[] chars = new char[16];
		int blocksOfFive = mins / 5;
		int remainingHours = mins % 5;
		int trackQuartHours = 0;
		for(int i = 0;i < 11; i++){
			if(blocksOfFive > 0){
				trackQuartHours = trackQuartHours + 5;
				if(trackQuartHours % 15 == 0){
					chars[i] = RED;
				} else {
					chars[i] = YELLOW;
				}
			} else {
				chars[i] = OFF;
			}
			blocksOfFive--;
		}
		chars[11] = ' ';
		for(int i = 12;i < 16; i++){
			if(remainingHours > 0){
				chars[i] = YELLOW;
			} else {
				chars[i] = OFF;
			}
			remainingHours--;
		}
		return new String(chars);
	}
}
