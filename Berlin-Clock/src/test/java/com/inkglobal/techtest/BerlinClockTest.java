package com.inkglobal.techtest;

import static org.junit.Assert.*;
import org.junit.Test;


public class BerlinClockTest {
	
	public static final int ONE_SECOND = 1;
	public static final int TWO_SECONDS = 2;
	
	public static final String HOURS_ZERO = "OOOO OOOO";
	public static final String HOURS_ONE = "OOOO ROOO";
	public static final String HOURS_SEVEN = "ROOO RROO";
	public static final String HOURS_THIRTEEN = "RROO RRRO";
	public static final String HOURS_TWENTYONE = "RRRR ROOO";
	public static final String HOURS_TWENTYFOUR = "RRRR RRRR";
	
	public static final String MINUTES_ZERO = "OOOOOOOOOOO OOOO";
	public static final String MINUTES_FIFTEEN = "YYROOOOOOOO OOOO";
	public static final String MINUTES_EIGTHEEN = "YYROOOOOOOO YYYO";
	public static final String MINUTES_THIRTY = "YYRYYROOOOO OOOO";
	public static final String MINUTES_FIFTYONE = "YYRYYRYYRYO YOOO";
	public static final String MINUTES_FIFTYNINE = "YYRYYRYYRYY YYYY";
	
	public static final String TIME_ZERO = "00:00:00";
	public static final String TIME_OUTPUT_ZERO = "Y OOOO OOOO OOOOOOOOOOO OOOO";
	
	public static final String TIME_THIRTEEN_SEVENTEEN_ONE = "13:17:01";
	public static final String TIME_OUTPUT_THIRTEEN_SEVENTEEN_ONE = "O RROO RRRO YYROOOOOOOO YYOO";
	
	public static final String TIME_TWENTYTHREE_FIFTYNINE_FIFYNINE = "23:59:59";
	public static final String TIME_OUTPUT_TWENTYTHREE_FIFTYNINE_FIFYNINE = "O RRRR RRRO YYRYYRYYRYY YYYY";
	
	public static final String TIME_TWENTY_FOUR_ZERO = "24:00:00";
	public static final String TIME_OUTPUT_TWENTY_FOUR_ZERO = "Y RRRR RRRR OOOOOOOOOOO OOOO";
	
	
	public BerlinClockTest() {
		
	}
	
	@Test
	public void shouldShowYellowLamp(){
		assertEquals(new Character(BerlinClock.YELLOW).toString(), BerlinClock.seconds(TWO_SECONDS));
	}
	
	@Test
	public void shouldShowLampOff(){
		assertEquals(new Character(BerlinClock.OFF).toString(), BerlinClock.seconds(ONE_SECOND));
	}
	
	@Test
	public void shouldShowTheCorrectMinutes(){
		assertEquals(MINUTES_ZERO, BerlinClock.minutes(0));
		assertEquals(MINUTES_FIFTEEN,BerlinClock.minutes(15));
		assertEquals(MINUTES_EIGTHEEN, BerlinClock.minutes(18));
		assertEquals(MINUTES_THIRTY, BerlinClock.minutes(30));
		assertEquals(MINUTES_FIFTYONE, BerlinClock.minutes(51));
		assertEquals(MINUTES_FIFTYNINE, BerlinClock.minutes(59));
		
	}
	
	@Test
	public void shouldShowTheCorrectHours(){
		assertEquals(HOURS_ZERO, BerlinClock.hours(0));
		assertEquals(HOURS_ONE, BerlinClock.hours(1));
		assertEquals(HOURS_SEVEN, BerlinClock.hours(7));
		assertEquals(HOURS_THIRTEEN, BerlinClock.hours(13));
		assertEquals(HOURS_TWENTYONE, BerlinClock.hours(21));
		assertEquals(HOURS_TWENTYFOUR, BerlinClock.hours(24));

	}
	
	@Test
	public void shouldShowTheCorrectTime(){
		assertEquals(TIME_OUTPUT_ZERO, BerlinClock.printBerlinClockStyleTime(TIME_ZERO));
		assertEquals(TIME_OUTPUT_TWENTY_FOUR_ZERO, BerlinClock.printBerlinClockStyleTime(TIME_TWENTY_FOUR_ZERO));
		assertEquals(TIME_OUTPUT_THIRTEEN_SEVENTEEN_ONE, BerlinClock.printBerlinClockStyleTime(TIME_THIRTEEN_SEVENTEEN_ONE));
		assertEquals(TIME_OUTPUT_TWENTYTHREE_FIFTYNINE_FIFYNINE, BerlinClock.printBerlinClockStyleTime(TIME_TWENTYTHREE_FIFTYNINE_FIFYNINE));
		
		
		
	}

}
